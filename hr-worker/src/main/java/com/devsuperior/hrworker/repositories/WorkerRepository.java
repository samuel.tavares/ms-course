package com.devsuperior.hrworker.repositories;

import com.devsuperior.hrworker.entities.Worker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Jpa21Utils;

public interface WorkerRepository extends JpaRepository<Worker, Long> {
}
